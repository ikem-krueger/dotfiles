#!/bin/bash
spectrconf="$HOME/.config/spectrwm/spectrwm.conf"
spectrpid="$(ps -aux | awk '/spectrwm$/ {print $2}')"
bgbar="$(grep /spectr-bgbar.sh $spectrconf | wc -l)"
if [ $bgbar = 1 ]; then
    sed -i 's/\/spectr-bgbar.sh/\/spectr-bar.sh/' $spectrconf && kill -HUP $spectrpid
else
    sed -i 's/\/spectr-bar.sh/\/spectr-bgbar.sh/'  $spectrconf && kill -HUP $spectrpid
fi

