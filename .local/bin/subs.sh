#!/bin/bash

# link to my videos page on youtube
url="https://www.youtube.com/c/linuxdabbler/videos"

# downloaded file location
file="$HOME/.config/subs.txt"

# get subscribers from downloaded file
getinfo="$(curl $url | sed 30q | tr , '\n' | grep subscribers | tail -n1 | cut -d':' -f 2 | sed 's|\}||; s|"||g' > $file)"


